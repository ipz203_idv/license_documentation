import Task7 from "./product/productApp";

function App() {
    return (
        <div className="App">
            <div>
                <h3 className="display-5 title">Lab07</h3>
                <Task7/>
            </div>
        </div>
    );
}

export default App;

