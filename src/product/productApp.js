import React, {useReducer} from 'react';
import reducer from './reducer'
import ProductForm from "./productForm";
import {ProductContext} from './productContext'


function ProductApp() {
    const [stateItem, dispatch] = useReducer(reducer, JSON.parse(localStorage.getItem('products')) || [])
    return (
        <ProductContext.Provider value={{dispatchItem: {stateItem, dispatch}}}>
            <ProductForm/>
        </ProductContext.Provider>
    );
}
export default ProductApp;