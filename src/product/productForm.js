import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import InputGroup from "react-bootstrap/InputGroup";
import React, {useContext, useEffect, useState} from "react";
import {ProductContext} from "./productContext";
import useDidMountEffect from "./useDidMountEffect";
import ProductList from "./productList";


export default function ProductForm() {

    const [ProductTitle, setProductTitle] = useState('')
    const [productAdd, setProductAdd] = useState('')


    const {dispatchItem} = useContext(ProductContext)

    useDidMountEffect(() => {
        alert("Додано: " + productAdd.toString())
    }, [productAdd]);


    useEffect(() => {
        localStorage.setItem('products', JSON.stringify(dispatchItem.stateItem))
    }, [dispatchItem.stateItem])


    const addProduct = () => {
        dispatchItem.dispatch({
            type: 'ADD_PRODUCT',
            payload: ProductTitle
        })
        setProductTitle('')
    }

    return (
        <div className="container">
            <div className="input-field">
                <InputGroup>
                    <Form.Control
                        type="text"
                        value={ProductTitle}
                        onChange={event => setProductTitle(event.target.value)}
                    />
                    <Button onClick={function () {
                        addProduct()
                        setProductAdd(ProductTitle)
                    }} variant="primary">Додати</Button>
                </InputGroup>
            </div>
            <ProductList products={dispatchItem.stateItem}/>
        </div>
    )
}