import React, {useContext, useState} from "react";
import {ProductContext} from "./productContext";
import ListGroup from "react-bootstrap/ListGroup";
import Button from "react-bootstrap/Button";

export default function Product({title, id}) {
    const {dispatchItem} = useContext(ProductContext)


    function handleCheckDelete(title) {
        return alert("Видалено: " + title)
    }

    const deleteProduct = () => {
        dispatchItem.dispatch({
            type: 'REMOVE_PRODUCT',
            payload: id,
        })
    }

    return (
        <ListGroup.Item as="li" className="d-flex justify-content-between align-items-start">
            <label>
                <span>{title}</span>
            </label>
            <Button variant="danger" style={{display: "absolute", position: "right"}}
                    onClick={function (){
                        handleCheckDelete(title)
                        deleteProduct()
                    }
                    }
            >
                🗑
            </Button>
        </ListGroup.Item>
    )
}